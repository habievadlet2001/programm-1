import {Layout} from "../../components/layout";
import {Row} from "antd";
import {EmployeeForm} from "../../components/employee-form";
import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {selectUser} from "../../features/authSlice";
import {useAddEmployeeMutation} from "../../app/services/employees";
import {Paths} from "../../paths";
import {isErrorWithMessage} from "../../utils/is-error-with-message";

export const AddEmployee = () => {
    const [error, setError] = useState("");
    const navigate = useNavigate();
    const user = useSelector(selectUser);
    const [addEmployee] = useAddEmployeeMutation();

    useEffect(() => {
        if (!user) {
            navigate('/login');
        }
    }, [navigate, user])

    const handleAddEmployee = async (data: Employee) => {
        try {
            await addEmployee(data).unwrap();

            navigate(`${Paths.status}/created`)
        } catch(err){
            const maybeError = isErrorWithMessage(err)

            if (maybeError) {
                setError(err.data.message)
            } else {
                setError('Неизвестная ошибка')
            }
        }
    }

    return (
        <Layout>
            <Row align="middle" justify='center'>
                <EmployeeForm onFinish={handleAddEmployee} btnText='Добавить' title='Добавить сотрудника' />
            </Row>
        </Layout>
    )
}